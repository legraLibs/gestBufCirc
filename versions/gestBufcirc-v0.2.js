/*!
fichier: bufCirc.js
version:0.2
auteur:pascal TOLEDO
date de creation: 2014.04.22
source: http://legral.fr/intersites/lib/perso/js/gestBufcirc
licence: GPLv3

description:
        * buffer circulaire. Le buffer se rempli en suivant un index qui se deplace circulairement. une fois la position MAX atteint il revient au debut et écrase l'entrée
        * fonction beginToZero: renvoie le contenu du buffer du plus ancien au plus recent sans le modifier

*/

const EOF= "\x04";


if(typeof(gestLib)==='object')
	gestLib.loadLib({nom:'gestBufCirc',ver:0.1,description:'buffer circulaire'})
else gestLib=null;
gestBufCirc=function(init)
	{
	if(!init){init={};}
	// -- var public -- //
	this.nom=init.nom?init.nom:'buffer'+((((1+Math.random())*0x10000)).toString().replace('.',''));//si pas de nom generer un uuid
	this.MAX=(isNaN(init.MAX))?10:init.MAX;		// Nombre d'entree maximum dans le buffer 
	this.dirSave=init.dirSave?init.dirSave:'./logs/';	
	this.pushBeforeSave=isNaN(init.pushBeforeSave)?this.MAX:init.pushBeforeSave;	// sauver le buffer tous les n push()

	// -- var internes -- //
	this.datas=[];// [00..MAX-1]
	this.nb=0;                                      // nb d'element dans data
	this.index=-1;
	this.pbs=this.pushBeforeSave;
	if(this.dirSave[this.dirSave.length-1]!='/')this.dirSave+='/';	// ajout du slash de fin

	try{this.fs=require('fs');}
	catch(e){this.fs=null;}	

	// -- configurationn automatique -- //
	
	// --- creation automatique du fichier log si non existant --- //
	if(this.fs){
		this.fs.mkdir(this.dirSave,function(){});	//creation du repertoire
		}

	if(init.autoload){
//		t="gestBufCirc:autoload demander";console.log(t.debug);
		this.load();
		}
	}


gestBufCirc.prototype=
	{
	// -- ajoute ou ecrase l'index courant avec la valeur sonnee en parametre -- //
	push:function(val)	// 
		{
		if(this.index<this.MAX-1){this.nb=++this.index;this.datas[this.nb]=val;}
		else{this.datas[this.index=0]=val;}
		this.pbs--;
//		t="pbs:"+this.pbs;console.log(t.data);
		if(this.pbs<=0){
			this.save();
			this.pbs=this.pushBeforeSave;
			}
		}

	// -- affiche des infos  sur le tableau -- //
	//	init.detail: 0|1 affiche les details du tableau 
	//	init.before:text (ou balise) ajouter au debut de chaque ligne
	//	init.after:text (ou balise) ajouter en fin de chaque ligne
	//-- //
	,show:function(init){
		if(typeof(init)!='object')init={};
		var tableau=init.tableau?init.tableau:this.datas;
		var before=init.before?init.before:'';
		var after=init.after?init.after:'';
		var out='';
		if(init.beginToZero)tableau=this.beginToZero();
		if(init.detail){
			out+=before+'nom='+this.nom+after;
			out+=before+'MAX='+this.MAX+after;
			out+=before+'nb='+tableau.length+after;
			out+=before+'index='+this.index+after;
			out+=before+'pushBeforeSave(actuel/consigne)='+this.pbs+'/'+this.pushBeforeSave+after;
			}

		for(var i=0;i<tableau.length;i++){
			out+=before;
			if(init.detail)out+=i+":";
			out+=tableau[i]+after;
			}

		return out;
		}

        // -- retourne un tableau avec le debut commencant a 0 -- //
        //      init.detail: 0|1 affiche les details du tableau 
        //      init.before:text (ou balise) ajouter au debut de chaque ligne
        //      init.after:text (ou balise) ajouter en fin de chaque ligne
        //-- //
        ,beginToZero:function(init){
                if(typeof(init)!='object')init={};
                var out=[],d=0;
                for(var i=this.index+1;i<this.datas.length;i++)out[d++]=this.datas[i];
                if(d<this.datas.length)for(var i=0;i<=this.index;i++)out[d++]=this.datas[i];
                return out;
                }

	// -- toLn(): renvoie le contenu sous forme de texte avec un retour a la ligne pour chaque entree du buffer
	,toLn:function(){
		var tosave='';
                for (var l=0;l<this.datas.length;l++){tosave+=this.datas[l]+"\n";}
		return(tosave);
		}
	,clear:function(){this.index=this.nb=0;this.data=[];}

	/* -- sauvegarde et resatration des buffers sur disque -- */

	// ---  save() --- //
	// return
	// 0 node present sauvegrade ok
	// 1 node present erreur lors de la sauvegarde
	// 2 node non present
	,save:function(){if(this.fs){
		t='bufCirc.js:save()//'+this.nom;console.log(t.debug);

		var fd=this.dirSave+this.nom+'.log';

		try{
//			this.fs.writeFile(fd, this.toLn(), function (err) {
			this.fs.appendFile(fd, this.toLn(), function (err) {
				if (err){console.log('bufCirc.js:save(): '+err);return 1;}
				console.log('ajout du buffer dans '+fd);
				return 0;
				});
			}
		catch(e){t="bufCirc.js:save():"+e;console.log(t.error);}
		}}

	// ---  load() --- //
	// charge le fichier de sauvegarde et remplie le buffer avec chaque ligne du fichier
	// param :aucun
        // return
        // 0 node present sauvegrade ok
        // 1 node present erreur lors de la sauvegarde
        // 2 node non present
        ,load:function(){if(this.fs){
		t='bufCirc.js:load() //'+this.nom;console.log(t.debug);

		var dl=''; // Dataload;
                var fd=this.dirSave+this.nom+'.log';	// File Destination

		try{
	                dl=this.fs.readFileSync(fd, {"encoding":"utf8"},function (err,_dl){
	                                if (err){t='bufCirc.js:load: '+err;console.log(t.error);return 1;}
					});
			}
		catch(e){t="bufCirc.js:load():"+e;console.log(t.error);}

		var c=0,
		ligneNb=0,
		tl='';	//text dans une ligne;

		for(c=0;c<dl.length;c++){
			if(dl[c]=="\n"){//retour a la  ligne raw:10
				this.push(tl);
				tl='';
				ligneNb++;
				continue;
				}
			tl+=dl[c];
			}

		return 0;

                }}

	}
/* ************************************************************
   ***********************************************************/
 // .end fait planter node
try{if(typeof(gestLib)==='object')gestLib.end('gestBufCirc');}catch(e){}

// activer en tant que module node.js
try{exports.gestBufCirc= gestBufCirc;}catch(e){}
