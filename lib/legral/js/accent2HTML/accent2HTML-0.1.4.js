/*!
fichier: accent2HTML.js
version:voir declaration gestLib
auteur:pascal TOLEDO
date: 2014.07.11
source: http://legral.fr/intersites/lib/legral/js/accent2HTML/
depend de:
  * voir README
description:
 * voir README
*/

if(typeof(gestLib)==='object')gestLib.loadLib({nom:'accent2HTML',ver:"0.1.3",description:'convertit les accents en code HTML'});else gestLib=null;

function accent2HTML(){
	{
	this.t=new Array();

	this.t['"']="&quot;" ;
	this.t["&"]="&amp;";
	this.t["€"]="&euro;";
	this.t["�"]="&#129;";
	this.t["‚"]="&#130;";
	this.t["ƒ"]="&#131;";
	this.t["„"]="&#132;";
	this.t["…"]="&#133;";
	this.t["†"]="&#134;";
	this.t["‡"]="&#135;";
	this.t["ˆ"]="&#136;";
	this.t["‰"]="&#137;";
	this.t["Š"]="&#138;";
	this.t["‹"]="&lt;";
	this.t["Œ"]="&#140;";
	this.t["�"]="&#141;";
	this.t["Ž"]="&#142;";
	this.t["�"]="&#143;";
	this.t["�"]="&#144;";
	this.t["‘"]="&#145;";
	this.t["’"]="&#146;";
	this.t["“"]="&#147;";
	this.t["”"]="&#148;";
	this.t["•"]="&#149;";
	this.t["–"]="&#150;";
	this.t["—"]="&#151;";
	this.t["˜"]="&#152;";
	this.t["™"]="&#153;";
	this.t["š"]="&#154;";
	this.t["›"]="&gt;";
	this.t["œ"]="&oelig;";
	this.t["�"]="&#157;";
	this.t["ž"]="&#158;";
	this.t["Ÿ"]="&Yuml;";
	this.t[" "]="&nbsp;";
	this.t["¡"]="&iexcl;";
	this.t["¢"]="&cent;";
	this.t["£"]="&pound;";
	this.t["¤"]="&curren;";
	this.t["¥"]="&yen";
	this.t["¦"]="&brvbar;";
	this.t["§"]="&sect;";
	this.t["¨"]="&uml;";
	this.t["©"]="&copy;";
	this.t["ª"]="&ordf;";
	this.t["«"]="&laquo;";
	this.t["¬"]="&not;";
	this.t["­"]="&shy;";
	this.t["®"]="&reg;";
	this.t["¯"]="&masr;";
	this.t["°"]="&deg;";
	this.t["±"]="&plusmn;";
	this.t["²"]="&sup2;";
	this.t["³"]="&sup3;";
	this.t["´"]="&acute;";
	this.t["µ"]="&micro;";
	this.t["¶"]="&para;";
	this.t["·"]="&middot;";
	this.t["¸"]="&cedil;";
	this.t["¹"]="&sup1;";
	this.t["º"]="&ordm;";
	this.t["»"]="&raquo;";
	this.t["¼"]="&frac14;";
	this.t["½"]="&frac12;";
	this.t["¾"]="&frac34;";
	this.t["¿"]="&iquest;";
	this.t["À"]="&Agrave;";
	this.t["Á"]="&Aacute;";
	this.t["Â"]="&Acirc;";
	this.t["Ã"]="&Atilde;";
	this.t["Ä"]="&Auml;";
	this.t["Å"]="&Aring;";
	this.t["Æ"]="&Aelig";
	this.t["Ç"]="&Ccedil;";
	this.t["È"]="&Egrave;";
	this.t["É"]="&Eacute;";
	this.t["Ê"]="&Ecirc;";
	this.t["Ë"]="&Euml;";
	this.t["Ì"]="&Igrave;";
	this.t["Í"]="&Iacute;";
	this.t["Î"]="&Icirc;" ;
	this.t["Ï"]="&Iuml;"; 
	this.t["Ð"]="&eth;";
	this.t["Ñ"]="&Ntilde;";
	this.t["Ò"]="&Ograve;";
	this.t["Ó"]="&Oacute;";
	this.t["Ô"]="&Ocirc;";
	this.t["Õ"]="&Otilde;";
	this.t["Ö"]="&Ouml;";
	this.t["×"]="&times;";
	this.t["Ø"]="&Oslash;";
	this.t["Ù"]="&Ugrave;";
	this.t["Ú"]="&Uacute;";
	this.t["Û"]="&Ucirc;";
	this.t["Ü"]="&Uuml;";
	this.t["Ý"]="&Yacute;";
	this.t["Þ"]="&thorn;";
	this.t["ß"]="&szlig;";
	this.t["à"]="&agrave;";
	this.t["Á"]="&Aacute;";
	this.t["Â"]="&Acirc;";
	this.t["Ã"]="&Atilde;";
	this.t["Ä"]="&Auml;";
	this.t["Å"]="&Aring;";
	this.t["Æ"]="&Aelig";
	this.t["Ç"]="&Ccedil;";
	this.t["È"]="&Egrave;";
	this.t["É"]="&Eacute;";
	this.t["Ê"]="&Ecirc;";
	this.t["Ë"]="&Euml;";
	this.t["Ì"]="&Igrave;";
	this.t["Í"]="&Iacute;";
	this.t["Î"]="&Icirc;";
	this.t["Ï"]="&Iuml;";
	this.t["Ð"]="&eth;";
	this.t["Ñ"]="&Ntilde;";
	this.t["Ò"]="&Ograve;";
	this.t["Ó"]="&Oacute;";
	this.t["Ô"]="&Ocirc;";
	this.t["Õ"]="&Otilde;";
	this.t["Ö"]="&Ouml;";
	this.t["×"]="&times;";
	this.t["Ø"]="&Oslash;";
	this.t["Ù"]="&Ugrave;";
	this.t["Ú"]="&Uacute;";
	this.t["Û"]="&Ucirc;";
	this.t["Ü"]="&Uuml;";
	this.t["Ý"]="&Yacute;";
	this.t["Þ"]="&thorn;";
	this.t["ß"]="&szlig;";
	this.t["à"]="&agrave;";
	this.t["á"]="&aacute;";
	this.t["â"]="&acirc;";
	this.t["ã"]="&atilde;";
	this.t["ä"]="&auml;";
	this.t["å"]="&aring;";
	this.t["æ"]="&aelig;";
	this.t["ç"]="&ccedil;";
	this.t["è"]="&egrave;";
	this.t["é"]="&eacute;";
	this.t["ê"]="&ecirc;";
	this.t["ë"]="&euml;";
	this.t["ì"]="&igrave;";
	this.t["í"]="&iacute;";
	this.t["î"]="&icirc;";
	this.t["ï"]="&iuml;";
	this.t["ð"]="&eth;";
	this.t["ñ"]="&ntilde;";
	this.t["ò"]="&ograve;";
	this.t["ó"]="&oacute;";
	this.t["ô"]="&ocirc;";
	this.t["õ"]="&otilde;";
	this.t["ö"]="&ouml;";
	this.t["÷"]="&divide;";
	this.t["ø"]="&oslash;";
	this.t["ù"]="&ugrave;";
	this.t["ú"]="&uacute;";
	this.t["û"]="&ucirc;";
	this.t["ü"]="&uuml;"; 
	this.t["ý"]="&yacute;";
	this.t["þ"]="&thorn;";
	this.t["ÿ"]="&yuml;";
	}
	this.convert=function(txt,space){
		var s=(space==0)?0:1;	// par defaut on ne convertie pas les espaces
		for (i=0;i<txt.length;i++){
			var c=txt.charAt(i);
			var t=this.t[c];
			if(c==' ' && space==0){out+=' ';continue;};
			if(t!=undefined){out+=t;continue;};
			out+=c;
			}
			
		return out;
		}
}

// - creation d'une instance globale - //
accent2HTML=new accent2HTML();

try{if(gestLib)gestLib.end('accent2HTML');}catch(e){}

// activer en tant que module node.js
try{exports.accent2HTML= accent2HTML;}catch(e){}

